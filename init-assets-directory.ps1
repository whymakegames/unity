$dirs = @(
	"animations",
	"editor",
	"materials",
	"prefabs",
	"scenes",
	"scripts",
	"textures"
);

foreach ($d in $dirs) {
	mkdir $d;
	$placeholder = "$d\.placeholder";
	"">>$placeholder;
}